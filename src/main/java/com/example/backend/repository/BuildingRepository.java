package com.example.backend.repository;

import com.example.backend.entity.BuildingDao;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface BuildingRepository extends JpaRepository<BuildingDao, UUID> {

}

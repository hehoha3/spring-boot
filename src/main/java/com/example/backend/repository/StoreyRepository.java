package com.example.backend.repository;

import com.example.backend.entity.StoreyDao;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface StoreyRepository extends JpaRepository<StoreyDao, UUID> {

}

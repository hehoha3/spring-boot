package com.example.backend.interceptor;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import java.util.Objects;

import org.apache.commons.codec.binary.Base64;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class Authenticator implements HandlerInterceptor {

    private JSONObject pubKeyJSON;
    private HttpURLConnection conn;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if (Objects.equals(request.getMethod(), "GET"))
        {
            return true;
        }
        pubKeyJSON = getPubKeyJSON();
        if (!isValid(request.getHeader("Authorization"))) {
            response.setStatus(403);
            System.out.println("Authentication failed. Token not valid");
            return false;
        }
        return true;
    }

    public JSONObject getPubKeyJSON() {
        BufferedReader reader;
        String line;
        StringBuilder responseContent = new StringBuilder();
        String response = "";
        try{
            URL url = new URL("http://keycloak:8080/auth/realms/biletado");
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(3000);
            conn.setReadTimeout(3000);

            int status = conn.getResponseCode();

            if (status >= 300) {
                reader = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
            }
            else {
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            }
            while ((line = reader.readLine()) != null) {
                responseContent.append(line);
            }
            reader.close();
            response = responseContent.toString();
        } catch (MalformedURLException e) {
            System.out.println("MalformedURLException");
        } catch (IOException e) {
            System.out.println("IOException");
        } finally {
            conn.disconnect();
        }
        return new JSONObject(response);
    }

    public RSAPublicKey getPubKeyObject(String pubKeyString) {
        byte[] encoded = Base64.decodeBase64(pubKeyString);
        try {
            KeyFactory kf = KeyFactory.getInstance("RSA");
            return (RSAPublicKey) kf.generatePublic(new X509EncodedKeySpec(encoded));
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            System.out.println("Publickey convert failed");
        }
        return null;
    }

    public boolean isValid(String authHeader) {
        String pubKeyString = pubKeyJSON.get("public_key").toString();
        String[] token = authHeader.split(" ");
        RSAPublicKey publicKey = getPubKeyObject(pubKeyString);
        try {
            Algorithm algorithm = Algorithm.RSA256(publicKey, null);
            JWTVerifier verifier = JWT.require(algorithm)
                    .build();
            verifier.verify(token[1]);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

}

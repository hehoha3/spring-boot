package com.example.backend.service;

import com.example.backend.entity.StoreyDao;

import java.util.List;
import java.util.UUID;

public interface StoreyService {

    List<StoreyDao> getAllStoreys();

    StoreyDao getStoreyById(UUID uuid);

    StoreyDao addNewStorey(String name, UUID building_id);

    StoreyDao updateStorey(UUID uuid, String name, UUID building_id);

    boolean deleteStorey(UUID uuid);

}

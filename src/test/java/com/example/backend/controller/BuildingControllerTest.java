package com.example.backend.controller;

import com.example.backend.entity.BuildingDao;
import com.example.backend.service.BuildingService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class BuildingControllerTest {

    @Mock
    private BuildingService mockedBuildingService;

    @Mock
    private BuildingDao mockedBuildingDao;

    @Test
    void getBuildingById() {
        UUID uuid = new UUID(123,456);

        Mockito.when(mockedBuildingService.getBuildingById(uuid)).thenReturn(mockedBuildingDao);
        BuildingDao bd = mockedBuildingService.getBuildingById(uuid);
        assertNotNull(bd);
    }
}